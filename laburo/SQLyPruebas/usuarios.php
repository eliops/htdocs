<?php
/*
VARIAS FUNCIONES USANDO SIMPLEXML
*/

//cargo un archivo xml que se encuentra dentro del dir

if(!$xml = simplexml_load_file('usuarios.xml')){
    echo "No se ha podido cargar el archivo";
} else {
    echo "El archivo se ha cargado correctamente";
}

//armo un xml y lo meto dentro de la variable xml_string
echo '<br>';
echo '<br>';

$xml_string = '<usuarios>
    <usuario>
        <nombre>Monnie</nombre>
        <apellido>Boddie</apellido>
        <direccion>Calle Pedro Mar 12</direccion>
        <ciudad>Mexicali</ciudad>
        <pais>Mexico</pais>
        <contacto>
            <telefono>44221234</telefono>
            <url>http://monnie.ejemplo.com</url>
            <email>monnie@ejemplo.com</email>
        </contacto>
    </usuario>
    </usuarios>';
if(!$xml = simplexml_load_string($xml_string)){
    echo "No se ha podido cargar el archivo en la variable xml_string";
} else {
    echo "Se ha cargado correctamente el archivo en la variable xml_string";
}

// Leo el archivo
echo '<br>';
echo '<br>';

if(!$xml = simplexml_load_file('usuarios.xml')){
    echo "No se ha podido cargar el archivo";
} else {
    foreach ($xml as $usuario){
        echo 'Nombre: '.$usuario->nombre.'<br>';
        echo 'Apellido: '.$usuario->apellido.'<br>';
        echo 'Dirección: '.$usuario->direccion.'<br>';
        echo 'Ciudad: '.$usuario->ciudad.'<br>';
        echo 'País: '.$usuario->pais.'<br>';
        echo 'Teléfono: '.$usuario->contacto->telefono.'<br>';
        echo 'Url: '.$usuario->contacto->url.'<br>';
        echo 'Email: '.$usuario->email->nombre.'<br>';
    }
}
?>