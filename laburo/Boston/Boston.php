<?php

//https://seguros.boston.com.ar/ws/emision/B2B.asmx

$u = 'aseguronline';
$p = 'cK2cfa1s9dc5i';
$userName = 'aseguronline';
$AppName = 'AseguroOnline';
$pais = 'ARG';
$target = 'Allianz';
$codAcc = '';
$nroPag = '1';
$cantReg = '15';

$xml = '
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="https://ws.boston.com.ar/ws">
   <soapenv:Header/>
   <soapenv:Body>
      <ws:Cotizar>
         <!--Optional:-->
         <ws:cotizacion>
            <!--Optional:-->
            <ws:Clave>7Gh2JFQpGztEL00PbgT5dVebytuMbOVJ</ws:Clave>
            <ws:CodigoAcuerdo>4013</ws:CodigoAcuerdo>
            <ws:CodigoTipoMovimiento>1</ws:CodigoTipoMovimiento>
            <ws:CodigoTipoEndoso>-1</ws:CodigoTipoEndoso>
            <ws:CodigoProductor>3956</ws:CodigoProductor>
            <ws:NumeroPolizaARenovar>1</ws:NumeroPolizaARenovar>
            <ws:NumeroPolizaAEndosar>1</ws:NumeroPolizaAEndosar>
            <ws:VigenciaDesde>2018-10-29</ws:VigenciaDesde>
            <ws:VigenciaHasta>9999-01-01</ws:VigenciaHasta>
            <ws:CodigoMoneda>0</ws:CodigoMoneda>
            <ws:CodigoTipoFacturacion>12</ws:CodigoTipoFacturacion>
            <ws:CodigoPlanDePago>9</ws:CodigoPlanDePago>
            <ws:CodigoProvincia>1</ws:CodigoProvincia>
            <ws:CodigoTipoDocumento>96</ws:CodigoTipoDocumento>
            <!--Optional:-->
            <ws:NumeroDocumento>30789385</ws:NumeroDocumento>
            <ws:CodigoCondicionIva>4</ws:CodigoCondicionIva>
            <ws:CodigoTipoPersona>1</ws:CodigoTipoPersona>
            <ws:CodigoTipoIngresosBrutos>2</ws:CodigoTipoIngresosBrutos>
            <!--Optional:-->
            <ws:JurisdiccionIngresosBrutos>
               <!--Zero or more repetitions:-->
               <ws:JurisdiccionIngresosBrutos>
                  <ws:CodigoProvincia>1</ws:CodigoProvincia>
                  <ws:Inscripto>false</ws:Inscripto>
                  <ws:Exceptuado>true</ws:Exceptuado>
               </ws:JurisdiccionIngresosBrutos>
            </ws:JurisdiccionIngresosBrutos>
            <!--Optional:-->
            <ws:FechaNacimiento>14/07/1984</ws:FechaNacimiento>
            <!--Optional:-->
            <ws:Ocupacion>Ingeniero</ws:Ocupacion>
            <ws:FactorDeRiesgo>1</ws:FactorDeRiesgo>
            <ws:AjusteComercial>1</ws:AjusteComercial>
            <!--Optional:-->
            <ws:Vehiculos>
               <ws:VehiculoCotizacion>
                  <ws:NumeroItem>1</ws:NumeroItem>
                  <ws:EsVehiculoClasico>0</ws:EsVehiculoClasico>
                  <ws:CodigoProvincia>2</ws:CodigoProvincia>
                  <ws:CodigoLocalidad>596</ws:CodigoLocalidad>
                  <!--Optional:-->
                  <ws:DescripcionLocalidad>Moron</ws:DescripcionLocalidad>
                  <ws:CodigoPostal>1708</ws:CodigoPostal>
                  <ws:CodigoInfoAuto>460540</ws:CodigoInfoAuto>
                  <!--Optional:-->
                  <ws:DescripcionMarca>Volkswagen</ws:DescripcionMarca>
                  <!--Optional:-->
                  <ws:DescripcionModelo>Voyage</ws:DescripcionModelo>
                  <ws:CodigoTipoVehiculo>8</ws:CodigoTipoVehiculo>
                  <ws:Anio>2012</ws:Anio>
                  <ws:Es0Km>0</ws:Es0Km>
                  <ws:SumaAsegurada>171000</ws:SumaAsegurada>
                  <!--Optional:-->
                  <ws:CodigoServicioAdicional>NO</ws:CodigoServicioAdicional>
                  <ws:TieneLocalizadorPropio>0</ws:TieneLocalizadorPropio>
                  <ws:CodigoUso>1</ws:CodigoUso>
                  <ws:CodigoCobertura>1</ws:CodigoCobertura>
                  <ws:Mercosur>1</ws:Mercosur>
                  <ws:RadioReducido>0</ws:RadioReducido>
                  <!--
                  <ws:CodigoMRA>26</ws:CodigoMRA>
                   -->
                  <ws:CodigoActividad>1</ws:CodigoActividad>
                  <ws:CodigoCarroceria>0</ws:CodigoCarroceria>
                  <ws:Franquicia>255</ws:Franquicia>
                  <!--Optional:-->
                  <ws:Accesorios>
                     <!--Zero or more repetitions:-->
                     <ws:Accesorio>
                        <ws:SumaAsegurada>10</ws:SumaAsegurada>
                        <ws:CodigoAccesorio>10</ws:CodigoAccesorio>
                     </ws:Accesorio>
                  </ws:Accesorios>
               </ws:VehiculoCotizacion>
            </ws:Vehiculos>
            <ws:AplicaBBR>0</ws:AplicaBBR>
         </ws:cotizacion>
      </ws:Cotizar>
   </soapenv:Body>
</soapenv:Envelope>
';

$location_URL = 'https://seguros.boston.com.ar/ws/emision/B2B.asmx';
// $location_URL = 'https://seguros.boston.com.ar/ws/emision/B2B.asmx?wsdl';

$client = new SoapClient(null, array(
    'location' => $location_URL,
    'uri' => $location_URL,
    'trace' => 1,
    // 'proxy_host' => "proxy01.aysa.ad",
    // 'proxy_port' => 80,
    // 'proxy_login' => "AYSA\I0603064",
    // 'proxy_password' => "Mayo2019",
));

$search_result = $client->__doRequest($xml, $location_URL, $location_URL, 1);

var_dump($search_result);
echo $search_result;


// $xml = new SimpleXMLElement($search_result);

// var_dump($xml);
// echo $xml->asXML();

// $xml->registerXPathNamespace('acc', 'http://xmlns.allianz.com.ar/Core/EBO/Allianz/AccesorioVehiculo');

// $resultado = $xml->xpath('//acc:AccesorioVehiculo');

// $i = 0;
// foreach ($xml->xpath('//acc:AccesorioVehiculo') as $resultado) {
//     echo " codigo: ";
//     echo $resultado->xpath('//acc:codigoAccesorio')[$i];
//     echo " descr: ";
//     echo $resultado->xpath('//acc:descripcionAccesorio')[$i];
//     echo "\n\n";
//     $i++;
//}
